import pandas as pd
from random import randint
import random
import datetime

path = "..//Data/"
coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv",sep=";")                             #creates a data frame with data from 2013 - 2017
new_coffeebar = pd.read_csv(path+"new_coffeebar.csv", sep=";")                              #creates a data frame for the data from 2018 - 2022

############################## PART 2: CREATING NEW CUSTOMER ###############################################################################

food_prices = {'Sandwich': 5,'Pie': 3,'Muffin': 3,'Cookie': 2, '': 0}                       #dictonnary with the food and their prices
drink_prices = {'Frappucino': 4,'Soda': 3,'Coffee': 3,'Tea': 3,'Water': 2,'Milkshake': 5}   #dictionnary with the drinks and their prices


                                                                #CLASSES



class Customers():                                                                          #super class Customers

    def __init__(self, customer_id):                                                        #Customer's attribut is its ID
        self.customer_id = customer_id

    def client_order(self):                                                                 #function that fill randomly the data frame with a drink and/or a food

        i = 0
        j = 0

        drink_list = ['Coffee', 'Frappucino', 'Soda', 'Tea', 'Water', 'Milkshake']
        food_list = ['Sandwich', 'Pie', 'Muffin', 'Cookie', '']

        while i != 312076 and  j != 312076:
            drink_order = random.choice(drink_list)
            new_coffeebar.loc[i,'DRINK'] = drink_order                                      #adds the random drink to the data frame
            i += 1
            print('food:', i)

            food_order = random.choice(food_list)
            new_coffeebar.loc[j,'FOOD'] = food_order                                        #adds the random food to the data frame (or nothing)
            j += 1
            print('drink:', j)

class Returning(Customers):                                                                 #class for the returning customers that inherits from Customers()

    def returningbill(self, food_order, drink_order):                                        #HUGE function to update a customer's budget based on its order
        self.food_order = food_order
        self.drink_drink = drink_order

        food_value = 0
        drink_value = 0
        print(r)

        if new_coffeebar.loc[r, 'CUSTOMER'] in new_coffeebar['CUSTOMER']:                   # check if the id in the dataframe2013-2017 is in the data frame 2018-2023, so that we can use the last budget
            budget = new_coffeebar.loc[r, 'BUDGET']
            print(True)
            print(budget)
        else:
            rand = random.uniform(0, 1)                                                     #to respect the constrain that 1/3 of the returning are regular and 2/3 are hipsters
            if rand <= 0.33:
                budget = 500                                                                #hipster's budget = 500
                new_coffeebar.loc[r, 'BUDGET'] = budget
            else:
                budget = 250                                                                #regular's budget = 250
                new_coffeebar.loc[r, 'BUDGET'] = budget

        drink = new_coffeebar.loc[r, 'DRINK']
        if drink in drink_prices:                                                           #takes the drink's price in drink_prices
            drink_value = drink_prices.get(drink)

        food = new_coffeebar.loc[r, 'FOOD']
        if food in food_prices:
            food_value = food_prices.get(food)                                              #takes the food's prince in food_prices

        bill = food_value + drink_value                                                     #calculate the amount of the bill

        if budget >= 10:                                                                    #checks if the customer still has a budget > 10
            budget = budget - bill                                                          #substract the bill's amount in the budget

        else:
            budget = budget                                                                 #if it's not the case, erase its order and budget is not updated
            new_coffeebar.loc[r, 'DRINK'] = ''
            new_coffeebar.loc[r, 'FOOD'] = ''

        new_coffeebar.loc[r, 'BUDGET'] = budget

class OneTime(Customers):                                                                   #class for the one time customers inherits from Customers()

    def onetimebill(self, food_order, drink_order):                                         #HUGE function to update a customer's budget
        self.food_order = food_order
        self.drink_drink = drink_order

        food_value = 0
        drink_value = 0
        print(r)

        budget = 100                                                                        #a onetimer's budget is 100
        new_coffeebar.loc[r, 'BUDGET'] = budget

        food = new_coffeebar.loc[r, 'FOOD']
        if food in food_prices:
            food_value = food_prices.get(food)                                              #takes the food's price in drink_prices

        drink = new_coffeebar.loc[r, 'DRINK']
        if drink in drink_prices:
            drink_value= drink_prices.get(drink)                                                   #takes the drink's price in drink_prices

        bill = food_value + drink_value                                                     #calculates the bill's amount
        if random.uniform(0.1, 0.9) <= 0.1:                                                 #to respect the constrain that 1/10 are TripAdvisor and 9/10 regular onetimers
            tip = randint(1, 10)                                                            #randomly calculates a tip
            budget = budget - bill - tip
        else:
            budget = budget - bill
        new_coffeebar.loc[r, 'BUDGET'] = budget                                             #update the budget in the data frame



def customer():                                                                             #function to assign an ID to a customer
    k = 0
    while k != 312076:
        if random.uniform(0, 1) <= 0.2:                                                     #to respect the probability 20% retuning and 80% onetimers
            customerid = random.choice(returningCustomer)                                   #randomly choose one of the returning in the list
            new_coffeebar.loc[k, 'CUSTOMER'] = customerid
        else:                                                                               #creates a new ID for a onetimer
            customerid = 'NID' + str(randint(0, 9)) + str(randint(0, 9)) + str(randint(0, 9)) + str(randint(0, 9)) + str(
                randint(0, 9)) + str(randint(0, 9)) + str(randint(0, 9))+ str(randint(0, 9))
            if (customerid not in coffeebar['CUSTOMER']) and (customerid not in new_coffeebar['CUSTOMER']):
                new_coffeebar.loc[k, 'CUSTOMER'] = customerid
        k += 1
        print('customer:', k)

returningCustomer = []                                                                      #list of all the retruning customer from the 1st data frame
for key, value in enumerate(coffeebar['CUSTOMER']):
    returningCustomer.append(value)
    if key == 999:
        break

#####################################PART 3: SIMULATION #################################################################################


print('start:',datetime.datetime.now())                                                 #to habe the time when the creatin of new data frame stars
n = 0
while n != 312074:
    customer()
    myClient = Customers(new_coffeebar['CUSTOMER'])                                         #creates our object/client
    myClient.client_order()
    n+=1

r = 0
for client in new_coffeebar['CUSTOMER']:                                                #code to assign the right budget to a customer depending on its customer's type
    if client not in returningCustomer:
        OneTime.onetimebill(new_coffeebar['CUSTOMER'], new_coffeebar['FOOD'], new_coffeebar['DRINK'])
    else:
        Returning.returningbill(new_coffeebar['CUSTOMER'], new_coffeebar['FOOD'], new_coffeebar['DRINK'])
    r += 1
    print('budget:', r)
    if r == 312076:
        break


print(new_coffeebar)
new_coffeebar.to_csv('NewCoffeebar')                                                    #Saves the data frame in an other CSV file to keep track of the client's order and budget
print('finish:',datetime.datetime.now())                                                # to know when new frame is finish so that we know how long it takes to biuld the data frame



