import pandas as pd
import matplotlib.pyplot as plt

path = "..//Data/"
coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv",sep=";")


def drinkSold():                                                            #returns the total drinks sold during the 5 years

    drinks = dict()
    for key,value in enumerate(coffeebar['DRINKS']):
        if value in drinks:
            drinks[value] += 1
        else:
            drinks[value] = 1
    print(drinks)
    total_drink = sum(drinks.values())
    print('The total drinks sold is: ',total_drink)


def foodSold():
    food = dict()
    for key,value in enumerate(coffeebar['FOOD']):
        if value != 'NaN':
            if value in food:
                food[value] += 1
            else:
                food[value] = 1
    print(food)
    total_food=(sum(food.values())-147805)
    print('The total food sold is: ',total_food)


def unique_customer():                                                      #Finds all the unique customers
    unique = coffeebar['CUSTOMER'].unique()                                 #function unique() that finds all the unique string in a list/dictionnary/...
    print('Here is an overview of the unique customers: ')
    print(len(unique))



def showPlot():                                                                 #function to create the plots
    NumberColumn = [1,2]
    total_column = [164270, 312075]

    LABELS = ["Total Foods", "Total Drinks"]

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Quantity')
    plt.title('Total foods & drinks')
    plt.show()


    NumberColumn = [1,2,3,4]
    total_column = [68623, 32010,31785,31852]

    LABELS = ['Sandwich', 'Pie','Muffin','Cookie']

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Qauntity')
    plt.title('Total foods 2013-2017')
    plt.show()

    NumberColumn = [1, 2, 3, 4, 5, 6]
    total_column = [40436, 95583,53833,40556,40915,40752]

    LABELS = ['Frappucino', 'Soda', 'Coffee', 'Tea', 'Water', 'Milkshake']

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Quantity')
    plt.title('Total drinks 2013-2017')
    plt.show()

def average(time, asked_drink, asked_food):                                                 #Calculates the probability

    drinks = {'frappucino': 0, 'soda': 0, 'coffee': 0, 'tea': 0, 'water': 0, 'milkshake': 0}
    foods = {'nan': 0, 'sandwich': 0, 'pie': 0, 'muffin': 0, 'cookie': 0}

    for elements in asked_drink:
        for row in coffeebar.values:
            if time == row[0][11:19]:                                                       #takes the line, slicing the date column and compares it to 'time' variable

                drink = row[2]                                                              #assign the drink from the data frame to the variable
                if (elements in drinks and elements == drink):
                    drinks[drink] += 1                                                      #+1 to the drink value in the dictionnary

                for elmts in asked_food:
                    food = row[3]
                    if (elmts in foods and elmts == food):
                        foods[food] += 1
                        print(foods)
                        print(drinks)

    average_drink = (drinks[asked_drink[0]] / 1825.0) * 100                                         #calculates the mean
    average_food = (foods[asked_food[0]] / 1825.0) * 100

    print("The average for %s you have asked is %d %%" % (asked_drink[0], average_drink))           #prints average for the drink
    print("And the average for %s you have asked is %d %%" % (asked_food[0], average_food))         #prints average for the food

