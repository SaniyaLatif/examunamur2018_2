import pandas as pd
import matplotlib.pyplot as plt

path = "..//Data/"
coffeebar = pd.read_csv(path+"Coffeebar_2013-2017.csv",sep=";")


def drinkSold():

    drinks = dict()
    for key,value in enumerate(coffeebar['DRINKS']):                #pour chaque indexe,valeur de ma colonne "dinks"
        if value in drinks:                                 #si la valeur est edja dans mon dico crée precedamment, je fait +1
            drinks[value] += 1
        else:                                                   #sinon j'ajoute une valeur dans mon dico qui a pour valeur 1
            drinks[value] = 1
    print(drinks)
    total_drink=sum(drinks.values())
    print("total drinks sold",total_drink)


def foodSold():
    food = dict()
    for key,value in enumerate(coffeebar['FOOD']):
        if value != 'NaN':
            if value in food:
                food[value] += 1
            else:
                food[value] = 1
    print(food)
    total_food=(sum(food.values())-147805)
    print("total food sold is",total_food)


def unique_customer():
    unique = coffeebar['CUSTOMER'].unique()
    print(len(unique))



def show_plot():
    NumberColumn = [1,2]
    total_column = [164270, 312075]

    LABELS = ["Total Foods", "Total Drinks"]

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Total quantity')
    plt.title('Total foods and drinks 2013-2017')
    plt.show()

    #histo: detail food

    NumberColumn = [1,2,3,4]
    total_column = [68623, 32010,31785,31852]

    LABELS = ["sandwich", "pie","muffin","cookie",]

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Qauntity')
    plt.title('Total foods 2013-2017')
    plt.show()

    #histo: detail drinks

    NumberColumn = [1,2,3,4,5,6]
    total_column = [40436, 95583,53833,40556,40915,40752]

    LABELS = ["frappucino", "soda","coffee","tea","water","milkshake"]

    plt.bar(NumberColumn, total_column, align='center')
    plt.xticks(NumberColumn, LABELS)

    plt.ylabel('Qauntity')
    plt.title('Total drinks 2013-2017')
    plt.show()

def show_probability(time, asked_drink, asked_food):

    #-> combien de food/drink a cette heure ci ?
    drinks = {"frappucino":0,"soda":0,'coffee':0,"tea":0,"water":0,'milkshake':0}
    foods = {'nan':0,"sandwich":0,"pie":0,"muffin":0,"cookie":0}
    for elements in asked_drink:
            for row in coffeebar.values: #pour chaque ligne de coffebar
                if time == row[0][11:19]:#je prends la ligne, le 2ieme elt correspondant à l'heure(just l'heure),et chech si == l'heure,si ok alors

                    drink = row[2] #assigne la boisson de la ligne à var boisson pour que ce soit plus parlant
                    if (elements in drinks and elements == drink):
                        drinks[drink] += 1 #dans mon dico boisson,fait +1 a la boisson correspondante
                    #else:
                        #print('Sorry, this drink is not available at the requested time')
                    for elmts in asked_food:
                        food = row[3]
                        if (elmts in foods and elmts == food):
                            foods[food] += 1
                            print(foods)
                            print(drinks)
                    #else:
                        #print('Sorry, this food is not available at the requested time')

#calcul moyenne :
    total_drinks = sum(drinks.values()) #somme boisons a cette heure-ci
    total_foods = sum(foods.values()) #somme food de cette heure ci

    average_drink = (drinks[asked_drink[0]]/1825.0)*100 #cherche la valeur de la boisson demandé ds mon dico
    average_food = (foods[asked_food[0]]/1825.0)*100

    print("The average for %s you have asked is %d %%" % (asked_drink[0], average_drink))
    print("And the average for %s you have asked is %d %%" % (asked_food[0], average_food))

drinkSold()
foodSold()
unique_customer()
show_plot()
show_probability('08:00:00',['coffee'],['muffin'])